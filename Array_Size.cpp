#include <iostream>

// --------- Main Function ------------- //
int main(){

    int arr[] = {1,2,3,4,5};
    int size {0};

    size = *(&arr + 1) - arr;

        std::cout << size << std::endl;

//-------------*** Another Way *** ------------------

    size = sizeof(arr)/sizeof(arr[0]);

    std::cout << size << std::endl;

    return 0;
}
